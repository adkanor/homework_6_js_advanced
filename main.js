"use strict";

//Асинхронность - возможность js выполнять орабатывать несколько запросов одновременно, не ожидая пока один запрос полностью не выполнится и не останавливая при этом код.
// Асинхронный код может обрабатывать запросы паралельно с загрузкой веб страницы
let button = document.querySelector(".button");
let userData = document.querySelector(".user-data");
let output = "";
const urlIp = "https://api.ipify.org/?format=json";

async function sendRequest(url) {
  let response = await fetch(url);
  let json = await response.json();
  return json;
}

class Info {
  constructor({ query, countryCode, country, region, city, regionName }) {
    this.query = query;
    this.countryCode = countryCode;
    this.country = country;
    this.region = region;
    this.city = city;
    this.regionName = regionName;
  }
  renderInfo() {
    output += `
    <h3>Here's what we could find on request with id: ${this.query}</h3>
    <p> Country code: ${this.countryCode}</p>
    <p>Country: ${this.country}</p>
    <p>Region: ${this.region}</p>
    <p>Region name: ${this.regionName}</p>

    <p>City: ${this.city}</p>
       `;
    userData.innerHTML = output;
  }
}

button.addEventListener("click", async () => {
  let personIp = await sendRequest(urlIp);
  const urlInfo = `http://ip-api.com/json/${personIp.ip}`;
  let personInfo = await sendRequest(urlInfo);
  new Info(personInfo).renderInfo();
});
